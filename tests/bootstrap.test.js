var sails = require('sails');
var Barrels = require('barrels');

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(5000);

  sails.lift({
    // configuration for testing purposes
	log: {
		level: 'error'
	},
	models: {
		connection : 'test',
		migrate: 'drop'
	}
  }, function(err) {
    if (err) return done(err);
    // here you can load fixtures, etc.

      // Load fixtures
      var barrels = new Barrels(process.cwd()+'/tests/fixtures');

      // Populate the DB
      barrels.populate(['product'], function(err) {
        done(err, sails);
      });
      
     // done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  console.log();
  sails.lower(done);
})
