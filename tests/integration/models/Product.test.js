assert = require('assert');
should = require('should');

describe('Product Model tests', function() {

  describe('#find()', function() {
    it('Find Function Test 1 : Find record matching a name', function (done) {
      Product.find({name:{'like':'%eleven%'}})
      .then(function(results) {
        assert.equal(results[0].id, 2, ['id is not 2']);
        assert.equal(results[0].quantity, 11, ['Quantity is not 11']);
	done();
      })
      .catch(function(err){
      	console.log(err);
	throw new Error(err);
      })
    });
    
    it('Find Function Test 2 : Try to find an empty result', function (done) {
      Product.find({name:{'like':'%twelve%'}})
      .then(function(results) {
	assert.equal(results.length, 0, ['Twelve record found']);
	done();
      })
      .catch(function(err){
      	console.log(err);
	throw new Error(err);
      })
    });
	
    

  });
  
  describe('#create()', function() {
    it('Create Function Test 1 : Check insertion', function (done) {
      var productData = {
	name : 'Test Product',
	description : 'Test Desc'
      };
      Product.create(productData)
      .then(function(result) {
	assert.equal(result.quantity, 1, ['quantity failed']);
	assert.equal(result.name, 'Test Product', ['Product name failed']);	
        done();
      })
      .catch(function(err){
      	console.log(err);
	throw new Error(err);
      })
      .done();
    });

    it('Create Function Test 2 : Check Description Required', function (done) {
      var productData = {
	name : 'Test Product'
      };
      Product.create(productData)
      .then(function(result) {
	throw new Error("Test Failed : Description was required");
      })
      .catch(function(err){
	done();
	console.log("\t \t Expected Failure for description not provided");
	//throw new Error("fail");
      })
      .done();
    });
    
    it('Create Function Test 3 : Check Name Required', function (done) {
      var productData = {
	description : 'Test Description'
      };
      Product.create(productData)
      .then(function(result) {
	throw new Error("Test Failed : Name was required");
      })
      .catch(function(err){
	done();
	console.log("\t \t Expected Failure for name not provided");
      })
      .done();
    });


  });
  
  describe('#update()', function() {
    it('Update Function Test 1 : Update record with id', function (done) {
      
      Product.update({id:1}, {name: 'Updated Name'})
      .then(function(results) {
	assert.notEqual(results.length, 0, ['No record updated']);
	assert.equal(results[0].name, 'Updated Name', ['Name update failed']);
	done();
      })
      .catch(function(err){
      	console.log(err);
	throw new Error(err);
      })
    });
    
  });
  
  describe('#destroy()', function() {
    it('Delete Function Test 1 : Delete Record with Updated Name', function (done) {
      Product.destroy({name:'Updated Name'})
      .then(function(results) {
	assert.notEqual(results.length, 0, ['No record deleted']);
	assert.equal(results[0].name, 'Updated Name', ['Records with Updated Name deleted']);
	done();
      })
      .catch(function(err){
      	console.log(err);
	throw new Error(err);
      })
    });
    
    
  });
  
});

