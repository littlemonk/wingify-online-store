# Design Document — RESTful API for managing products of an online store #

On this page:

1. [Goal](#markdown-header-goal)
2. [Application Design](#markdown-header-application-design)
3. [Data Design](#markdown-header-data-design)
4. [Implementation](#markdown-header-implementation)
5. [Authentication](#markdown-header-authentication)
6. [API in action](#markdown-header-api-in-action)
7. [Tests](#markdown-header-tests)
8. [Automating Setup](#markdown-header-automating-setup)

--- 
## Goal ##

Provide a RESTful API endpoint to be consumed by a mobile developer for managing products of online store. The API endpoint should supoort addition, deletion, editing and searching options for a product. 

Only authenticated users should be able to perfom permitted actions. 

---
### Application Design

Our application will have primarily four methods for each of the actions on the product. 

* Addition of a product 

Given a `POST` request on our api end point with valid data object we will store the data in our database and will return a HTTP status code `201 Created` with object stored in database.

Otherwise we will return a HTTP status code `400 Bad Request` will be returned with a object containing request details which caused it to fail.

* Editing a product

To edit a product, send the `PUT` request on our api end-point with the resource id and updated data object.

If the requests succeds we will return a HTTP status code `200 OK` with updated data object in database.

Otherwise we will return a HTTP status code `400 Bad Request` will be returned with a object containing request details which caused it to fail.

* Searching a product

To search a product, send a `GET` request on our api end-point with product and name.

If the requests succeds we will return a HTTP status code `200 OK` with found product, if it exists.

* Deleting a product

To delete a product, send a `DELETE` request on our api end-point with product id.

If the request succeds we will return a HTTP status code `200 OK` with deleted object from data store.

---
### Data Design

As the problem statement allows us to assume anything about our data schema, we will be going with a very simple example. Our product data will have following components : 

* id 
* name
* description
* quantity 

We are keeping our data constrains fair and simple. Name and descriptions are compulsory strings while quantity is an integer which will be initialised to 0 if not given. 


In ideal scenario we will use some relational database to store this product data. In our implementation we will be using a persistent on disk object store(sails-disk).

For mysql database the create table statement would be like :

```
CREATE TABLE `product` (                                                                                                     
  `id` int(11) NOT NULL AUTO_INCREMENT,                                                                                                  
  `name` varchar(60) NOT NULL,                                                                                                           
  `description` varchar(250) NOT NULL,                                                                                                   
  `quantity` int(11) NOT NULL DEFAULT '0',                                                                                               
  PRIMARY KEY (`id`)                                                                                                                     
)
```

---
### Implementation 

We are using [sails.js](sailsjs.com) framework for writing sample application. Our application will have a single endpoint `/product`. 

For this application we will be using Blueprint API for implementing the RESTful API. 

* [Blueprint API Concept](http://sailsjs.com/documentation/concepts/blueprints)
* [Blueprint API Refrence](http://sailsjs.com/documentation/reference/blueprint-api)

> Blueprints are Sails’ way of quickly generating API routes and actions based on your application design.  

To generate a Blueprint api having `/product` we have to run `sails generate api product` which will create follwing files - 

* `api/controllers/ProductController.js`
* `api/models/Product.js`

Sails ships with [Waterline](https://github.com/balderdashy/waterline) ORM. Waterline provides a uniform API for storing and accessing data from different kind of databases like Redis, MySQL or MongoDB. It does this by using `adapaters` which is translates the uniform API to different data store. In this implementation we will be using [sails-disk](https://github.com/balderdashy/sails-disk/). It is disk based object store.

##### Models 

A model represents a collection of structured data. They are usually define in sails app's `api/models` folder.  Here we will be defining our data object attributes in file `api/models/Product.js`.

```javascript
 attributes: {
    productId : {
        type: 'int',
        int:true,
    },

    name : { 
        type: 'string',
        string: true,
        required:true,
    },

    description : { 
        type: 'string',
        string:true,
        required:true,
    },

    quantity : { 
        type: 'int',
        int:true,
        defaultsTo : '1',
    } 
 }

```

We have generated a Blueprint API for `/product` end point and we have also added our data schema at the end of our API actions.

---
### Authentication 

In a ideal real life scenario, we will register users in our database and will make a privilege system on top of it. The privilege system will allow administrators to assign, edit or remove permissions to users.

For implementation here we are using a header based authentication - only requests having header 'X-Auth' with a pre determined value  will be proccessed. Else - we will send a 403 - forbidden response.

To authenticate a request to access controller actions sails uses `policies`. They are located in `api/polcies` folder and contain authentication logic. We have created a new policy in `api/policies/isAuthenticated.js` file and wrote our logic of allowing a request to only proceed if its 'X-Auth' header contains valid value. Otherwise it will send a forbidden response. 

Next we added to check for this policy for every action of ProductController in `config/policies.js`. Now we have made sure that only authorized users will be using the ProductController.

---
### API in action

The app is deployed on heroku at https://wingify-online-store.herokuapp.com/.

#### Get product

* get all product

Request 
```
GET /product HTTP/1.1
Host: wingify-online-store.herokuapp.com
Content-Type: application/json
X-Auth: animesh
Cache-Control: no-cache
```
Success Response 

```
Status 200 OK

[
  {
    "name": "Mobile",
    "description": "This is a test mobile",
    "quantity": "1",
    "createdAt": "2017-02-15T14:33:10.355Z",
    "updatedAt": "2017-02-15T14:33:10.355Z",
    "id": 1
  },
  {
    "name": "Mobile2",
    "description": "This is another test mobile",
    "quantity": "111",
    "createdAt": "2017-02-15T14:33:30.298Z",
    "updatedAt": "2017-02-15T14:44:00.321Z",
    "id": 2
  }
]

```
![all product](nonsvn/api-requests-shots/get-all-item.png)

 * search for a product 
```
GET /product?name=Mobile HTTP/1.1
Host: wingify-online-store.herokuapp.com
Content-Type: application/x-www-form-urlencoded
X-Auth: animesh
Cache-Control: no-cache
```
Success response 

```
Status 200 OK
[
  {
    "name": "Mobile",
    "description": "This is a test mobile",
    "quantity": "1",
    "createdAt": "2017-02-15T14:33:10.355Z",
    "updatedAt": "2017-02-15T14:33:10.355Z",
    "id": 1
  }
]
```
![search for a product](nonsvn/api-requests-shots/get-single-item.png)


#### Create product
    
* create product success 

```
POST /product HTTP/1.1
Host: wingify-online-store.herokuapp.com
Content-Type: application/x-www-form-urlencoded
X-Auth: animesh
Cache-Control: no-cache

name=This+is+newly+created+Product&quantity=10&description=This+the+new+description
```

Success Response 

```
Status 201 Created
{
  "name": "This is newly created Product",
  "quantity": "10",
  "description": "This the new description",
  "createdAt": "2017-02-20T06:09:21.605Z",
  "updatedAt": "2017-02-20T06:09:21.605Z",
  "id": 3
}
```

![create success](nonsvn/api-requests-shots/post-success-201.png)


* create product fail

```
POST /product HTTP/1.1
Host: localhost:1337
X-Auth: animesh
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache

name=Mobile2&quantity=111

```

Failure response 

```
Status 400 Bad Request 

{
  "invalidAttributes": {
    "description": [
      {
        "rule": "string",
        "message": "Value should be a string (instead of null, which is an object)"
      },
      {
        "rule": "string",
        "message": "\"string\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined"
      },
      {
        "rule": "required",
        "message": "\"required\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined"
      }
    ]
  },
  "reason": "1 attribute is invalid",
  "code": "E_VALIDATION",
  "status": 400,
  "details": "Invalid attributes sent to Product:\n • description\n   • Value should be a string (instead of null, which is an object)\n   • \"string\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined\n   • \"required\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined\n",
  "message": "[Error (E_VALIDATION) 1 attribute is invalid] Invalid attributes sent to Product:\n • description\n   • Value should be a string (instead of null, which is an object)\n   • \"string\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined\n   • \"required\" validation rule failed for input: null\nSpecifically, it threw an error.  Details:\n undefined\n"
}

```
![create fail](nonsvn/api-requests-shots/post-400-message.png)



#### Edit product
```
PUT /product/3 HTTP/1.1
Host: wingify-online-store.herokuapp.com
Content-Type: application/x-www-form-urlencoded
X-Auth: animesh
Cache-Control: no-cache

name=This+is+newly+created+Product+with+updated+name&quantity=10&description=This+the+new+description
```
Response object 

```
Status 200 OK
{
  "name": "This is newly created Product with updated name",
  "quantity": "10",
  "description": "This the new description",
  "createdAt": "2017-02-20T06:09:21.605Z",
  "updatedAt": "2017-02-20T06:12:48.614Z",
  "id": 3
}
```
![update  success](nonsvn/api-requests-shots/update-200.png)

#### Delete product
```
DELETE /product/3 HTTP/1.1
Host: wingify-online-store.herokuapp.com
Content-Type: application/json
X-Auth: animesh
Cache-Control: no-cache
```
Response object 

```
Status 200 OK

{
  "name": "This is newly created Product with updated name",
  "quantity": "10",
  "description": "This the new description",
  "createdAt": "2017-02-20T06:09:21.605Z",
  "updatedAt": "2017-02-20T06:12:48.614Z",
  "id": 3
}
```
![delete success](nonsvn/api-requests-shots/delete-200.png)

---

### Tests

Sails uses [mocha](https://mochajs.org/) test suite for testing purpose. We will only be testing our model since we are using Blueprint API and we are not overriding default actions of Blueprint.

Our tests are written in `tests` folder. The file `tests/bootstrap.test.js` initializes the required modules and the tests are written in `tests/integration` folder. Our particular test cases for `api/models/Product.js` are written in `tests/integration/models/Product.test.js`. 

We are testing constraints on our data schema on the model methods - `find`, `create`, `update` and `destroy`.

To run the tests - use `npm test`.

### Automating setup

We will be using `Ansible` to set up and deploy this app on a server. All the ansible scripts are written in folder [`ansible-setup`](ansible-setup) of this repo. 

There are two roles in our deployment setup:

* node - Installs dependencies, nodejs and npm on a fresh server

* store-app-deploy - Pulls the code on destination server and starts the node server 

Example commands - 

To set up this app on local machine in `wingify-app` folder using ansible - run `ansible-playbook -i "ansible-setup/inventories/production/web-server"  ansible-setup/app-setup.yml`

Assuming node and npm are already installed and to only deploy the app - run `ansible-playbook -i "ansible-setup/inventories/production/web-server"  ansible-setup/app-setup.yml --tags "deploy-app"`