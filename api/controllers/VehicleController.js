/**
 * VehicleController
 *
 * @description :: Server-side logic for managing vehicles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	setMapUserVehicle : function (req, res) {
		console.log(req);
		var userId =  req.param('userId');
		var vehicleId = req.param('vehicleId');
		VehicleUserMap.create({vehicleId: vehicleId, userId : userId}).exec(function (err, result){
			if (err) {
				return res.send(err);
			};
			res.send(result);
		});
	},

	//get all mapped user vehicles
	getMapUserVehicle : function (req, res) {
		VehicleUserMap.find({}).exec(function(err, mapData){
			if (err) {
				return res.send(err);
			};
			res.send(mapData);
		});	
	}

};

