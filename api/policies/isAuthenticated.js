/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
var APPROVED_API_KEY = 'animesh'; // temporary.  TODO: use db or similar.

module.exports = function checkAuth(req, res, next) {
	var auth_header = req.headers['x-auth'];
	if (auth_header == APPROVED_API_KEY) {
		return next();
	}
	return res.forbidden('You are not permitted to perform this action.');
}
