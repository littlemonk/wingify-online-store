/**
 * VehicleUserMap.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName : 'VEHICLE_USER_MAP',
  attributes: {
	vehicleId: {
		type : 'int',
		required : true,
		numeric : true
	},
	userId: {
		type : 'int',
		required : true,
		numeric : true
	},
  }
};

