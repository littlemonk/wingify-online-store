/**
 * Vehicle.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName : 'TEST_VEHICLE',
  attributes: {
	vehicleId: 'INT',
	vehicleMake: {
		type : 'STRING',
		required : true
	},
	vehicleModel: 'STRING',
	vehicleName : {
		type : 'string',
		defaultsTo: 'Alto',
		unique : 'true' 
	}
  }
};

