/**
 * Product.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    productId : { 
	type: 'int',
	int:true,
	},

    name : { 
	type: 'string',
	string: true,
	required:true,
	},

    description : { 
	type: 'string',
	string:true,
	required:true,
	},

    quantity : { 
	type: 'int',
	int:true,
	defaultsTo : '1',
	} 
  }
};

